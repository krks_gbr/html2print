


import $ from 'jquery';
import cssRegions from '../css-regions';
import masterPageHTML from './html/masterpage.html';
import './style/main.less';




import defaultConfig from './h2p.config.default.json';


let H2P = function({html, postProcess}){


    //container
    let $pages = $('<div>').attr('id', 'pages').addClass('mirrored');


    let $contentWrapper =
        $('<div>')
            .attr('id', 'content-source')
            .html(html)
            .appendTo('body');


    let $masterPageTemplate = $(
        $.parseHTML(masterPageHTML).find(el => $(el).hasClass('masterPage'))
    );


    //create pages
    for (let i = 0; i<50; i++){
        let page = $masterPageTemplate
            .clone()
            .attr("id", "page-" + i).appendTo($pages);
        page.find('.body').addClass('content-target');
    }


    $pages.prependTo('body');


    return new Promise((resolve, reject) => {
        cssRegions.enablePolyfill();
        let flow = document.getNamedFlow('contentflow');
        flow.addEventListener('regionfragmentchange', () => {

            postProcess($pages);

            let empty =
                $pages
                    .find('.paper')
                    .filter( (i, paper)  => {
                        return $(paper).find('cssregion').children().length === 0
                    });

            empty.remove();

            let wait = setInterval(function(){
                let isFlowFinished = !flow.relayoutInProgress && !flow.relayoutScheduled;
                if(isFlowFinished){
                    resolve();
                    clearInterval(wait);
                }
            }, 100);


        });
    });

};

window.H2P = H2P;
export default H2P;