
const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');



const defaultConfig = {
    devtool: "source-map",
    module:{
        loaders:[

            { test: /\.(png|jpg|woff|woff2|eot|ttf|svg)$/, loader: 'url?limit=100000' },

            {
                test: /\.html$/,
                loader: "html",
            },

            {
                test: /\.json$/,
                loader: 'json'
            },

            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract("style",`css!resolve-url!postcss!sass`),
            },

            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract("style",`css!resolve-url!postcss!less`),
            },

            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract("style",`css!resolve-url!postcss`)
            },

            {
                test: /\.jsx?$/,
                loaders: ["babel"],
                include: [ `${__dirname}/src/` ]
            }
        ]
    },

    postcss: function(){
        return [autoprefixer];
    },

    plugins: [
        new webpack.optimize.DedupePlugin()
    ]

};

const mainConfig = Object.assign({},
    defaultConfig,
    {

        entry: `${__dirname}/src/h2p/index.js`,
        output: {
            path: `${__dirname}/dist/`,
            filename: 'bundle.js',
        },
        plugins: defaultConfig.plugins.concat(  new ExtractTextPlugin("style/style.bundle.css") ),
        module:{
            loaders: defaultConfig.module.loaders.concat(
                {
                    test: /\.jsx?$/,
                    loaders: ["babel"],
                    include: [ `${__dirname}/src/` ]
                }
            )
        }
    }
);

const exampleConfig = Object.assign({},
    defaultConfig,
    {

        entry: `${__dirname}/example/src/index.js`,
        output: {
            path: `${__dirname}/example/dist/`,
            filename: 'bundle.js',
        },
        plugins: defaultConfig.plugins.concat(  new ExtractTextPlugin("style/style.bundle.css") ),
        module:{
            loaders: defaultConfig.module.loaders.concat(
                {
                    test: /\.jsx?$/,
                    loaders: ["babel"],
                    include: [ `${__dirname}/example/src` ]
                }
            )
        }
    }
);


module.exports = [mainConfig, exampleConfig];