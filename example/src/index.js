
import $ from 'jquery';
import './style/style.css';

window.onload = function(){



    let ps = $('p');
    let regex = /".*?"/g;

    ps.each(function(){
        let _this = this;
        let matches = $(this).text().match(regex);
        if(matches){
            matches.forEach(function(match){
                let element = $('<span>').text(match).addClass('quote')[0].outerHTML;
                let newHTML =  $(_this).html().replace(match, element);
                $(_this).html(newHTML);
            });
        }
    });



    let images = [      "DWave_128chip.jpg",
                        "minivac-completo.jpg",
                        "server_room.jpg",
                        "Ssc2003-06c.jpg",
                        "multivac.jpg",
                        "susanne_posel_news_-super-computer-321243-910x512.jpg"
    ];



    images.forEach(function(src){


        $('<img>').attr('src', 'imgs/' + src).insertAfter(  ps.get(  Math.floor( Math.random() * ps.length  )   )   );

    });



    const postProcess   = $pages => {

        $pages.find('.paper .footer').each((i, footer) => $(footer).text(`${i+1}`));
        $pages.find('.paper .header').each((i, header) => $(header).text(`header ${i+1}`));
    };


    H2P({
        html: $('#wrapper').html(),
        postProcess: postProcess,
    });
    console.log(window.H2P);


};
